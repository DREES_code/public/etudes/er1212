# ER1212

Ce dossier fournit les programmes permettant de produire les illustrations, les chiffres et les tableaux complémentaires de l'*Études et résultats* n° 1212 de la DREES, publié le 21/10/2021 : "Allocation personnalisée d’autonomie : la part de l’espérance de vie passée en tant que bénéficiaire diminue depuis 2010".

Les programmes permettent aussi de produire, sous la forme d'un rapport RMarkdown, une analyse plus détaillée et des illustrations complémentaires.

Données sources utilisées, producteurs : enquête Aide sociale (DREES) ; bilan démogaphique (INSEE). Traitements : DREES.

Lien vers l'étude : https://drees.solidarites-sante.gouv.fr/publications/etudes-et-resultats/allocation-personnalisee-dautonomie-la-part-de-lesperance-de-vie

Présentation de la DREES : La Direction de la recherche, des études, de l'évaluation et des statistiques (DREES) est le service statistique ministériel des ministères sanitaires et sociaux, et une direction de l'administration centrale de ces ministères.
https://drees.solidarites-sante.gouv.fr/article/presentation-de-la-drees

Date de la dernière exécution des programmes avant publication, et version des logiciels utilisés : Les programmes ont été exécutés pour la dernière fois avec le logiciel R version 4.0.5, le 07/10/2021.
